import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListPokemonsComponent } from './modules/pages/list-pokemons/list-pokemons.component';
import { CreatePokemonComponent } from './modules/pages/create-pokemon/create-pokemon.component';
import { EditPokemonComponent } from './modules/pages/edit-pokemon/edit-pokemon.component';
import { DeletedPokemonComponent } from './modules/pages/deleted-pokemon/deleted-pokemon.component';
const routes: Routes = [
  { path:'', redirectTo:'pokemons', pathMatch:'full'},
  { path:'pokemons', component:ListPokemonsComponent},
  { path:'create-pokemon', component:CreatePokemonComponent},
  { path:'edit-pokemon/:id', component:EditPokemonComponent},
  { path:'delete-pokemon/:id', component:DeletedPokemonComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
