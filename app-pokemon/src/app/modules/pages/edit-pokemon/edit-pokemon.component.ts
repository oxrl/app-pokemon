import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder,ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Pokemon } from '../../../shared/models/pokemon';
import { PokemonService } from '../../../core/services/pokemon.service';
import { TrigerPokemonService } from '../../../core/services/triger-pokemon.service';


@Component({
  selector: 'app-edit-pokemon',
  templateUrl: './edit-pokemon.component.html',
  styleUrls: ['./edit-pokemon.component.css']
})
export class EditPokemonComponent implements OnInit {

  pokemon: any;
  editForm = new FormGroup({
    id: new FormControl(''),
    name: new FormControl(''),
    image: new FormControl(''),
    type: new FormControl(''),
    hp: new FormControl(''),
    attack: new FormControl(''),
    defense: new FormControl(''),
    idAuthor: new FormControl('')
  });

  constructor(private fb: FormBuilder,private router: Router,private _trigerPokemonService:TrigerPokemonService ,private activatedRoute: ActivatedRoute, private _pokemonService: PokemonService) { }

  ngOnInit(): void {
    let id = JSON.parse(this.activatedRoute.snapshot.paramMap.get('id')!);
    this.getByIdPokemons(id);
  }

  getByIdPokemons(id: number) {
    this._pokemonService.getByIdPokemons(id).subscribe(data => {      
      this.pokemon = data;
      this.editForm = this.fb.group(
        {
          id :  [this.pokemon.id],
          name :  [this.pokemon.name],
          image: [this.pokemon.image],
          type: [this.pokemon.type],
          hp: [this.pokemon.image],
          attack: [this.pokemon.attack],
          defense: [this.pokemon.defense],
          idAuthor: [ this.pokemon.idAuthor]
        }
      );
  
    });
  }
  return() {
    this._trigerPokemonService.triggerPokemon.emit({
      showHeaderEmit:false
      });
      this.router.navigate(['pokemons']);
  }
  setInputName() {
    var name = this.editForm.value.name;
    var txtName = document.getElementById('name');
    if (txtName != null) {
      txtName.innerHTML = name;      
    }
  }

  valueAttack() {
    var slider1 = <HTMLInputElement>document.getElementById('slider1');
    var val1 = document.getElementById('value1');
    if (val1 != null) {
      val1.innerHTML = slider1.value;
      this.pokemon.attack = parseInt(slider1.value);
    }
  }

  valueDefense() {
    var slider = <HTMLInputElement>document.getElementById('slider');
    var val = document.getElementById('value');
    if (val != null) {
      val.innerHTML = slider.value;
      this.pokemon.defense = parseInt( slider.value);
    }
  }

  postForm(form:Pokemon){

    if (form.name=="") {
      alert('Ingresar el nombre del Pokémon');
    }else if (form.image=="") {
      alert('Ingresar la url del Pokémon');
    }
     else {
      this._pokemonService.putPokemons(form).subscribe(data =>{        
        alert('Pokémon editado exitosamnete');
        this.router.navigate(['pokemons']);
        this._trigerPokemonService.triggerPokemon.emit({
          showHeaderEmit:false
          })
      });
    }
  }
}
