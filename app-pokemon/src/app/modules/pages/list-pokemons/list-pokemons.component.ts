import { Component, OnInit } from '@angular/core';
import { Pokemon } from '../../../shared/models/pokemon';
import { PokemonService } from '../../../core/services/pokemon.service';
import { TrigerPokemonService } from '../../../core/services/triger-pokemon.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-pokemons',
  templateUrl: './list-pokemons.component.html',
  styleUrls: ['./list-pokemons.component.css']
})
export class ListPokemonsComponent implements OnInit {
  pokemons:Pokemon[]=[];
  search:number=0;

  constructor(private _pokemonService:PokemonService,private _trigerPokemonService:TrigerPokemonService ,private router:Router) {
    this._trigerPokemonService.triggerPokemon.subscribe(
      data => {
        this.pokemons=data.pokemonsEmit;
      }
    )
   }

  ngOnInit(): void {
    this.getAllPokemons();
  }


  getAllPokemons() {
    this._pokemonService.getPokemons().subscribe(data =>{
      this.pokemons = data;
    });
  }

}
