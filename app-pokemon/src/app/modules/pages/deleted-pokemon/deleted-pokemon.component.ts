import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-deleted-pokemon',
  templateUrl: './deleted-pokemon.component.html',
  styleUrls: ['./deleted-pokemon.component.css']
})
export class DeletedPokemonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
