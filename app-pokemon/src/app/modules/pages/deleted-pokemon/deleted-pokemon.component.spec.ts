import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletedPokemonComponent } from './deleted-pokemon.component';

describe('DeletedPokemonComponent', () => {
  let component: DeletedPokemonComponent;
  let fixture: ComponentFixture<DeletedPokemonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeletedPokemonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletedPokemonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
