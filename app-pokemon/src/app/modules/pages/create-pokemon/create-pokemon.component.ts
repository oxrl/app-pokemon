import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Pokemon } from '../../../shared/models/pokemon';
import { PokemonService } from '../../../core/services/pokemon.service';
import { TrigerPokemonService } from 'src/app/core/services/triger-pokemon.service';
@Component({
  selector: 'app-create-pokemon',
  templateUrl: './create-pokemon.component.html',
  styleUrls: ['./create-pokemon.component.css']
})
export class CreatePokemonComponent implements OnInit {

  newForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    image: new FormControl('', [Validators.required]),
    type: new FormControl('fire'),
    hp: new FormControl(100),
    attack: new FormControl(0),
    defense: new FormControl(0),
    idAuthor: new FormControl(1),
  });

  constructor(private router: Router, private _trigerPokemonService:TrigerPokemonService,private _pokemonService: PokemonService) { }

  ngOnInit(): void {
  }

  return() {
    this.router.navigate(['pokemons']);
    this.trigerEmit();
  }
  trigerEmit(){
    this._trigerPokemonService.triggerPokemon.emit({
      showBtnCreateEmit:false
      })
  }
  valueAttack() {
    var slider1 = <HTMLInputElement>document.getElementById('slider_1');
    var val1 = document.getElementById('value_1');
    if (val1 != null) {
      val1.innerHTML = slider1.value;
    }
  }

  valueDefense() {
    var slider = <HTMLInputElement>document.getElementById('slider_2');
    var val = document.getElementById('value_2');
    if (val != null) {
      val.innerHTML = slider.value;
    }
  }
  enviarForm(form:Pokemon){
    if (form.name=="") {
      alert('Por favor ingresar el nombre del Pokémon');
    }else if (form.image=="") {
      alert('Por favor ingresar la url del Pokémon');
    }
     else {
      this._pokemonService.postPokemon(form).subscribe(data =>{
        alert('El Pokémon se ha creado exitosamente');
        this.router.navigate(['pokemons']);
        this.trigerEmit();
      });
    }

  }
}