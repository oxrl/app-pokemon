import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from '../app-routing.module';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ListPokemonsComponent } from './pages/list-pokemons/list-pokemons.component';
import { SearchButtonComponent } from './components/search-button/search-button.component';
import { CreateButtonComponent } from './components/create-button/create-button.component';
import { UpdateButtonComponent } from './components/update-button/update-button.component';
import { DeleteButtonComponent } from './components/delete-button/delete-button.component';
import { CreatePokemonComponent } from './pages/create-pokemon/create-pokemon.component';
import { EditPokemonComponent } from './pages/edit-pokemon/edit-pokemon.component';

@NgModule({
  declarations: [CreatePokemonComponent,EditPokemonComponent,ListPokemonsComponent, SearchButtonComponent, CreateButtonComponent, UpdateButtonComponent, DeleteButtonComponent],
  imports: [  
    BrowserModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,    
    AppRoutingModule
    
  ],
  exports : [ListPokemonsComponent,SearchButtonComponent,CreatePokemonComponent,CreateButtonComponent,DeleteButtonComponent],
})
export class ModulesModule { }