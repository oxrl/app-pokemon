import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonService } from 'src/app/core/services/pokemon.service';
import { TrigerPokemonService } from 'src/app/core/services/triger-pokemon.service';
import { Pokemon } from 'src/app/shared/models/pokemon';

@Component({
  selector: 'app-delete-button',
  templateUrl: './delete-button.component.html',
  styleUrls: ['./delete-button.component.css']
})
export class DeleteButtonComponent implements OnInit {
  @Input() idPokemon:any; 
  pokemons:Pokemon[]=[];
  constructor(private _pokemonService:PokemonService,private _trigerPokemonService:TrigerPokemonService,private router:Router) { }

  ngOnInit(): void {
    this.router.navigate(['pokemons']);
  }
  pokemonDelete(idPokemon:number){
    this._pokemonService.deletePokemon(idPokemon).subscribe(data =>{
    alert('Se Eliminó el Pokemón');
    location.reload();      
    });
  }
}
