import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from '../../../shared/models/pokemon';
import { PokemonService } from '../../../core/services/pokemon.service';
import { TrigerPokemonService } from '../../../core/services/triger-pokemon.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-update-button',
  templateUrl: './update-button.component.html',
  styleUrls: ['./update-button.component.css']
})
export class UpdateButtonComponent implements OnInit {
  @Input() idPokemon:any;
  @Input() showBtnSearchEmit: boolean = true;
  pokemons:Pokemon[]=[];
  constructor(private _pokemonService:PokemonService,private _trigerPokemonService:TrigerPokemonService,private router:Router) { }

  ngOnInit(): void {
  }
  pokemonEdit(idPokemon:Input){ 
    this._trigerPokemonService.triggerPokemon.emit({
      showBtnSearchEmit:this.showBtnSearchEmit
      })
 
    this.router.navigate(['edit-pokemon', idPokemon]);
  }

}
