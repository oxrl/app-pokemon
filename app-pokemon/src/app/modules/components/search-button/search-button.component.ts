import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from '../../../shared/models/pokemon';
import { PokemonService } from '../../../core/services/pokemon.service';
import { TrigerPokemonService } from '../../../core/services/triger-pokemon.service';

@Component({
  selector: 'app-search-button',
  templateUrl: './search-button.component.html',
  styleUrls: ['./search-button.component.css']
})
export class SearchButtonComponent implements OnInit {
  pokemons:Pokemon[]=[];
  search:number=0;
  @Input() pokemonsEmit:any;
  showBtnSearchEmit: boolean = false;
  showBtnCreateEmit: boolean = false;
  
  constructor(private _pokemonService:PokemonService,private _trigerPokemonService:TrigerPokemonService) {
    this._trigerPokemonService.triggerPokemon.subscribe(
      data => {
        this.showBtnSearchEmit=data.showBtnSearchEmit;
        this.showBtnCreateEmit=data.showBtnCreateEmit;
      }
    )
   }

  ngOnInit(): void {
  }
  getByIdPokemons() {
    this.pokemons = [];
 
      if(this.search == null){
        this.search=0;
      }
      this._pokemonService.getByIdPokemons(this.search).subscribe(data => {
        let res = new Object();
        res = data;
        if(data == null || data == undefined ){
         res = {};
        }; 
        if(Object.keys(res).length > 0 ){                    
        this.pokemons.push(data);
        this.pokemonsEmit= this.pokemons;
        this._trigerPokemonService.triggerPokemon.emit({
        pokemonsEmit:this.pokemonsEmit
        }) 
      } else {
        alert("No existe algun Pokémon con esos datos");
        this._pokemonService.getPokemons().subscribe(data =>{
          this.pokemons = data;
          this.pokemonsEmit= this.pokemons;
          this._trigerPokemonService.triggerPokemon.emit({
            pokemonsEmit:this.pokemonsEmit
            })
        });
      }
      },
      error => {
        alert("No existe un problema con la conexion");     
      });
    
    

    

  }

}
