import { Component, Input, OnInit } from '@angular/core';
import { TrigerPokemonService } from '../../../core/services/triger-pokemon.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-create-button',
  templateUrl: './create-button.component.html',
  styleUrls: ['./create-button.component.css']
})
export class CreateButtonComponent implements OnInit {
  @Input() showBtnCreateEmit: boolean = true; 
  constructor(private router:Router,private _trigerPokemonService:TrigerPokemonService) { }

  ngOnInit(): void {
  }
  pokemonAdd(){
    this.router.navigate(['create-pokemon']);
    this._trigerPokemonService.triggerPokemon.emit({
      showBtnCreateEmit:this.showBtnCreateEmit
      })
  }
}
