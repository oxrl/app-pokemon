import { TestBed } from '@angular/core/testing';

import { TrigerPokemonService } from './triger-pokemon.service';

describe('TrigerPokemonService', () => {
  let service: TrigerPokemonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TrigerPokemonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
