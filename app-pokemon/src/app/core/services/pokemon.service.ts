import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Pokemon} from '../../shared/models/pokemon'; //Interfaz 

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  url:string= "https://bp-pokemons.herokuapp.com/"

  constructor(private http:HttpClient) { }

  getPokemons():Observable <Pokemon[]> {
    let direccion = this.url+"?idAuthor=1";
    return this.http.get<Pokemon[]>(direccion);
  }

  getCountPokemons():Observable <number> {
    let direccion = this.url+"count?idAuthor=1";
    return this.http.get<number>(direccion);
  }

  getByIdPokemons(id:number|null):Observable <Pokemon> {
    let direccion = this.url+id;
    return this.http.get<Pokemon>(direccion);

  }

  putPokemons(form:Pokemon):Observable <any> {   
    const params = JSON.stringify(form); 
    let direccion = this.url+'?id='+form.id;
   // alert("direccion: "+ direccion);
   // alert("params: "+params);
    return this.http.put(direccion, params,  { headers : this.getHeaders()});
  }

  deletePokemon(id:any):Observable <Pokemon> {
    let direccion = this.url+id;
    return this.http.delete<Pokemon>(direccion);
  }

  postPokemon(form:Pokemon):Observable <any> {
    let direccion = this.url+"?idAuthor="+form.idAuthor;
    //alert("direccion: "+ direccion);
    //alert("form: "+ JSON.stringify(form));
    return this.http.post<Pokemon>(direccion, form);
  }
  
  public getHeaders(){
    return new HttpHeaders().set('Content-Type','application/json');
  }
}
