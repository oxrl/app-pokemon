import { EventEmitter, Injectable, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class TrigerPokemonService {
 @Output() triggerPokemon: EventEmitter<any> = new EventEmitter(); 
 @Output() triggerShowBtnSearch: EventEmitter<any> = new EventEmitter(); 
 @Output() showBtnCreateEmit: EventEmitter<any> = new EventEmitter(); 
  constructor(private http:HttpClient) { }

 
}
