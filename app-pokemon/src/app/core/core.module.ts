import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './header/header.component';
import { ModulesModule } from '../modules/modules.module';

@NgModule({
  declarations: [
    HeaderComponent
  ],
  exports: [ HeaderComponent ],
  imports: [
    CommonModule,
    ModulesModule,
    HttpClientModule
  ]
})
export class CoreModule { }
